package com.masterpik.games.bungee.events;

import com.masterpik.api.util.UtilArrayList;
import com.masterpik.api.util.UtilString;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Collections;
import java.util.List;

public class GamesCoMessageEvent extends Event {
    public static final HandlerList handlers = new HandlerList();

    protected String server;
    protected String player;
    protected List<String> datas;

    public GamesCoMessageEvent(String server, String player) {
        this.server = server;
        this.player = player;

        this.datas = Collections.emptyList();
    }

    public GamesCoMessageEvent(String server, String player, String datas) {
        this.server = server;
        this.player = player;
        this.datas = Collections.emptyList();
        this.datas.addAll(UtilString.CryptedStringToArrayList(datas));
    }

    public GamesCoMessageEvent(String server, String player, List<String> datas) {
        this.server = server;
        this.player = player;

        this.datas = Collections.emptyList();
        this.datas.addAll(datas);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    public String getServer() {
        return server;
    }

    public String getPlayer() {
        return player;
    }

    public Player getBukkitPlayer() {
        Player bPlayer = Bukkit.getPlayer(this.player);
        if (bPlayer != null && bPlayer.isValid() && bPlayer.isOnline()) {
            return bPlayer;
        } else {
            return null;
        }
    }

    public List<String> getDatas() {
        return datas;
    }
}
