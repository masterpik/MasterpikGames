package com.masterpik.games.settings;

public class Settings {

    public static void init() {
        GeneralSettings.init();
        GamesSettings.init();
        ScoreboardSettings.init();
        WorldSettings.init();
        LobbySettings.init();
        NetworkSettings.init();
    }

}
