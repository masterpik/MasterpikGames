package com.masterpik.games.settings;

public class NetworkSettings {

    private static long cleanTimeTick;

    public static void init() {
        cleanTimeTick = 20 * 60 * 5;
    }

    public static long getCleanTimeTick() {
        return cleanTimeTick;
    }
    public static void setCleanTimeTick(long cleanTimeTick) {
        NetworkSettings.cleanTimeTick = cleanTimeTick;
    }
}
