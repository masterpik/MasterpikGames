package com.masterpik.games.settings;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import java.util.ArrayList;

public class LobbySettings {

    private static World lobbyWorld;
    private static Location lobbyLocation;
    private static boolean canSwapItem;
    private static boolean canBreakBlock;
    private static ArrayList<Material> breakingBlock;

    private static GameMode gameMode;

    public static void init() {
        lobbyWorld = null;
        lobbyLocation = null;
        canSwapItem = true;
        canBreakBlock = false;

        breakingBlock = new ArrayList<>();

        gameMode = GameMode.ADVENTURE;
    }

    public static World getLobbyWorld() {
        return lobbyWorld;
    }
    public static void setLobbyWorld(World lobbyWorld) {
        LobbySettings.lobbyWorld = lobbyWorld;
    }

    public static Location getLobbyLocation() {
        return lobbyLocation;
    }
    public static void setLobbyLocation(Location lobbyLocation) {
        LobbySettings.lobbyLocation = lobbyLocation;
    }

    public static boolean isCanSwapItem() {
        return canSwapItem;
    }
    public static void setCanSwapItem(boolean canSwapItem) {
        LobbySettings.canSwapItem = canSwapItem;
    }

    public static boolean isCanBreakBlock() {
        return canBreakBlock;
    }
    public static void setCanBreakBlock(boolean canBreakBlock) {
        LobbySettings.canBreakBlock = canBreakBlock;
    }

    public static ArrayList<Material> getBreakingBlock() {
        return breakingBlock;
    }
    public static void setBreakingBlock(ArrayList<Material> breakingBlock) {
        LobbySettings.breakingBlock = breakingBlock;
    }

    public static GameMode getGameMode() {
        return gameMode;
    }
    public static void setGameMode(GameMode gameMode) {
        LobbySettings.gameMode = gameMode;
    }
}
