package com.masterpik.games.settings;

public class GeneralSettings {

    private static boolean canSwapItem;

    public static void init() {
        canSwapItem = true;
    }

    public static boolean isCanSwapItem() {
        return canSwapItem;
    }
    public static void setCanSwapItem(boolean canSwapItem) {
        GeneralSettings.canSwapItem = canSwapItem;
    }
}
