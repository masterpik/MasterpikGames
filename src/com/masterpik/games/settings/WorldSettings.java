package com.masterpik.games.settings;

import org.bukkit.GameMode;
import org.bukkit.Material;

import java.util.ArrayList;

public class WorldSettings {

    private static boolean canBreakBlock;
    private static ArrayList<Material> breakingBlock;
    private static GameMode gameMode;

    public static void init() {
        canBreakBlock = true;

        breakingBlock = new ArrayList<>();

        gameMode = GameMode.SURVIVAL;
    }

    public static boolean isCanBreakBlock() {
        return canBreakBlock;
    }
    public static void setCanBreakBlock(boolean canBreakBlock) {
        WorldSettings.canBreakBlock = canBreakBlock;
    }

    public static ArrayList<Material> getBreakingBlock() {
        return breakingBlock;
    }
    public static void setBreakingBlock(ArrayList<Material> breakingBlock) {
        WorldSettings.breakingBlock = breakingBlock;
    }

    public static GameMode getGameMode() {
        return gameMode;
    }
    public static void setGameMode(GameMode gameMode) {
        WorldSettings.gameMode = gameMode;
    }
}
