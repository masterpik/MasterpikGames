package com.masterpik.games.players;

import com.masterpik.api.players.MasterpikPlayer;
import com.masterpik.games.games.Game;
import com.masterpik.games.teams.Team;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class GamesPlayer extends MasterpikPlayer {

    protected Game games;
    protected Team team;

    protected boolean valid;
    protected boolean spectate;

    protected HashMap<String, Object> customs;

    public GamesPlayer(MasterpikPlayer clone) {
        super(clone);
        this.games = null;
        this.team = null;

        valid = false;
        spectate = false;


        customs = new HashMap<>();
    }

    @Override
    public Player getPlayer() {
        return super.getBukkitPlayer();
    }

    public Game getGames() {
        return games;
    }
    public void setGames(Game games) {
        this.games = games;
    }

    public Team getTeam() {
        return team;
    }
    public void setTeam(Team team) {
        this.team = team;
    }

    public boolean isValid() {
        return valid;
    }
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isSpectate() {
        return spectate;
    }
    public void setSpectate(boolean spectate) {
        this.spectate = spectate;
    }

    public HashMap<String, Object> getCustoms() {
        return customs;
    }
    public void setCustoms(HashMap<String, Object> customs) {
        this.customs = customs;
    }
}
