package com.masterpik.games.players;

import com.masterpik.api.players.Players;
import com.masterpik.games.MasterpikGames;
import com.masterpik.games.bungee.events.GamesCoMessageEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class PlayersManager {

    public static HashMap<UUID, GamesPlayer> players;

    public static HashMap<String, GamesCoMessageEvent> playersDatas;

    public static void init() {
        players = new HashMap<>();
        playersDatas = new HashMap<>();

        Bukkit.getScheduler().runTaskLater(MasterpikGames.getPlugin(), new Runnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    playerJoin(player.getUniqueId());
                }
            }
        }, 200);
    }

    public static void playerJoin(UUID uuid) {
        if (Players.players.containsKey(uuid)) {
            players.put(uuid, new GamesPlayer(Players.players.get(uuid)));
        }
    }

    /*public static GamesPlayer playerJoin(UUID uuid) {
        if (Players.players.containsKey(uuid)) {
            players.put(uuid, new GamesPlayer(Players.players.get(uuid)));
        }
        return players.get(uuid);
    }*/

    public static void playerQuit(UUID uuid) {
        if (players.containsKey(uuid)) {
            players.remove(uuid);
        }
    }

    public static void validJoin(GamesPlayer player) {
        if (playersDatas.containsKey(player.getPlayer().getName())) {



            //TODO finish function

            playersDatas.remove(player.getPlayer().getName());
            player.setValid(true);
        }
    }
}
