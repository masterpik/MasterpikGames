package com.masterpik.games.listeners;

import com.masterpik.games.listeners.bungee.ForwardMessage;
import com.masterpik.games.listeners.bungee.GamesCoMessage;
import com.masterpik.games.listeners.player.PlayerJoin;
import com.masterpik.games.listeners.player.PlayerQuit;
import com.masterpik.games.listeners.world.BlockBreak;
import com.masterpik.games.listeners.player.PlayerSwapHandItems;

public class Listeners {


    public static void init() {
        ForwardMessage.init();
        GamesCoMessage.init();

        PlayerJoin.init();
        PlayerQuit.init();
        PlayerSwapHandItems.init();

        BlockBreak.init();
    }

}
