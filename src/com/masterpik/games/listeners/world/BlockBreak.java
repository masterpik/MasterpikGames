package com.masterpik.games.listeners.world;

import com.masterpik.games.MasterpikGames;
import com.masterpik.games.settings.LobbySettings;
import com.masterpik.games.settings.WorldSettings;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreak implements Listener {

    public static void init() {
        Bukkit.getPluginManager().registerEvents(new BlockBreak(), MasterpikGames.getPlugin());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerBlockBreakEvent(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if (player.getWorld().equals(LobbySettings.getLobbyWorld())
                && (!LobbySettings.isCanBreakBlock() || LobbySettings.getBreakingBlock().contains(block.getType()))) {
            event.setCancelled(true);
        } else if (!WorldSettings.isCanBreakBlock() || WorldSettings.getBreakingBlock().contains(block.getType())){
            event.setCancelled(true);
        }

    }

}
