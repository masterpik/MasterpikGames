package com.masterpik.games.listeners.bungee;

import com.masterpik.api.events.custom.messaging.ForwardMessageEvent;
import com.masterpik.api.util.UtilString;
import com.masterpik.games.MasterpikGames;
import com.masterpik.games.bungee.events.GamesCoMessageEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;

public class ForwardMessage implements Listener {

    public static void init() {
        Bukkit.getPluginManager().registerEvents(new ForwardMessage(), MasterpikGames.getPlugin());
    }

    @EventHandler
    public void ForwardMessageEvent(ForwardMessageEvent event) {
        if (event.getChannel().equalsIgnoreCase("BungeeCord")
                && event.getSubChannel().equalsIgnoreCase("gamesCo")) {
            ArrayList<String> datas = new ArrayList<>();
            datas = UtilString.CryptedStringToArrayList(event.getMessage());
            ArrayList<String> args = new ArrayList<>();
            if (datas.size() > 2) {
                args = (ArrayList<String>) datas.clone();
                args.remove(datas.get(0));
                args.remove(datas.get(1));
            }
            GamesCoMessageEvent msgEvent = new GamesCoMessageEvent(datas.get(0), datas.get(1), args);
            Bukkit.getPluginManager().callEvent(msgEvent);
        }
    }
}
