package com.masterpik.games.listeners.bungee;

import com.masterpik.games.MasterpikGames;
import com.masterpik.games.bungee.events.GamesCoMessageEvent;
import com.masterpik.games.players.PlayersManager;
import com.masterpik.games.settings.NetworkSettings;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class GamesCoMessage implements Listener {
    public static void init() {
        Bukkit.getPluginManager().registerEvents(new GamesCoMessage(), MasterpikGames.getPlugin());
    }

    @EventHandler
    public void GamesCoMessageEvent(GamesCoMessageEvent event) {
        if (!PlayersManager.playersDatas.containsKey(event.getPlayer())) {
            if (!(MasterpikGames.get(event.getPlayer()) != null && MasterpikGames.get(event.getPlayer()).isValid())) {
                PlayersManager.playersDatas.put(event.getPlayer(), event);

                Bukkit.getScheduler().runTaskLater(MasterpikGames.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        if (PlayersManager.playersDatas.containsKey(event.getPlayer())) {
                            PlayersManager.playersDatas.remove(event.getPlayer());
                        }
                    }
                }, NetworkSettings.getCleanTimeTick());
            }
        }
    }
}
