package com.masterpik.games.listeners.player;

import com.masterpik.games.MasterpikGames;
import com.masterpik.games.settings.GeneralSettings;
import com.masterpik.games.settings.LobbySettings;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

public class PlayerSwapHandItems implements Listener{

    public static void init() {
        Bukkit.getPluginManager().registerEvents(new PlayerSwapHandItems(), MasterpikGames.getPlugin());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerSwapHandItemsEvent(PlayerSwapHandItemsEvent event) {
        Player player = event.getPlayer();
        World world = player.getWorld();

        if (GeneralSettings.isCanSwapItem()) {
            if (LobbySettings.getLobbyWorld() != null
                    && LobbySettings.getLobbyWorld().equals(world)
                    && !LobbySettings.isCanSwapItem()) {
                event.setCancelled(true);
            }
        } else {
            event.setCancelled(true);
        }

    }

}
