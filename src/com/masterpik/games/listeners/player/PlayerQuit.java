package com.masterpik.games.listeners.player;

import com.masterpik.games.MasterpikGames;
import com.masterpik.games.players.PlayersManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit implements Listener {

    public static void init() {
        Bukkit.getPluginManager().registerEvents(new PlayerQuit(), MasterpikGames.getPlugin());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        PlayersManager.playerQuit(event.getPlayer().getUniqueId());
    }
}
