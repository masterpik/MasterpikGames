package com.masterpik.games.listeners.player;

import com.masterpik.api.Networks.Servers;
import com.masterpik.api.util.UtilMessaging;
import com.masterpik.games.MasterpikGames;
import com.masterpik.games.players.GamesPlayer;
import com.masterpik.games.players.PlayersManager;
import com.masterpik.games.settings.LobbySettings;
import com.masterpik.games.settings.NetworkSettings;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;

public class PlayerJoin implements Listener {

    public static void init() {
        Bukkit.getPluginManager().registerEvents(new PlayerJoin(), MasterpikGames.getPlugin());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PlayersManager.playerJoin(player.getUniqueId());
        GamesPlayer gamesPlayer = MasterpikGames.get(player);

        event.setJoinMessage(null);

        if (PlayersManager.playersDatas.containsKey(player.getName())) {
            PlayersManager.validJoin(gamesPlayer);
        }
        else {

            Location tpBcl = LobbySettings.getLobbyLocation();
            if (tpBcl == null) {
                tpBcl = Bukkit.getWorlds().get(0).getSpawnLocation();
            }


            player.teleport(tpBcl);

            ArrayList<Integer> waiters = new ArrayList<Integer>();
            final Location finalTpBcl = tpBcl;
            long runTime = 20 * 1;
            waiters.add(Bukkit.getScheduler().scheduleSyncRepeatingTask(MasterpikGames.getPlugin(), new Runnable() {

                long bcl = 0;

                @Override
                public void run() {
                    if (bcl <= NetworkSettings.getCleanTimeTick()) {
                        if (PlayersManager.playersDatas.containsKey(player.getName())) {

                            PlayersManager.validJoin(gamesPlayer);

                            Bukkit.getScheduler().cancelTask(waiters.get(0));

                        } else {
                            player.teleport(finalTpBcl);
                        }

                        bcl = bcl + runTime;
                    } else {
                        UtilMessaging.sendPlayer(player, Servers.HUB.getName());
                        Bukkit.getScheduler().cancelTask(waiters.get(0));
                    }
                }
            }, runTime, runTime));
        }
    }

}
