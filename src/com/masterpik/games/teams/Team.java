package com.masterpik.games.teams;

import com.masterpik.games.games.Game;
import com.masterpik.games.players.GamesPlayer;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class Team {

    protected int id;
    protected byte color;

    protected Game game;

    protected HashMap<Player, GamesPlayer> players;

    protected Location respawnLocation;


    protected HashMap<String, Object> customs;


    public Team(int id, byte color, Game game, Location respawnLocation) {
        this.id = id;
        this.color = color;

        this.game = game;

        this.players = new HashMap<>();

        this.respawnLocation = respawnLocation;

        this.customs = new HashMap<>();
    }


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public byte getColor() {
        return color;
    }
    public void setColor(byte color) {
        this.color = color;
    }


    public Game getGame() {
        return game;
    }
    public void setGame(Game game) {
        this.game = game;
    }


    public HashMap<Player, GamesPlayer> getPlayers() {
        return players;
    }
    public void setPlayers(HashMap<Player, GamesPlayer> players) {
        this.players = players;
    }


    public Location getRespawnLocation() {
        return respawnLocation;
    }
    public void setRespawnLocation(Location respawnLocation) {
        this.respawnLocation = respawnLocation;
    }


    public HashMap<String, Object> getCustoms() {
        return customs;
    }
    public void setCustoms(HashMap<String, Object> customs) {
        this.customs = customs;
    }
}
