package com.masterpik.games.games;

import com.masterpik.games.players.GamesPlayer;
import com.masterpik.games.teams.Team;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class Game {

    protected int id;
    protected String name;

    protected World world;

    protected int nbTeams;
    protected int nbTeamsPlayers;

    protected int category;
    protected int subCategory;
    protected int subSubCategory;

    protected HashMap<Player, GamesPlayer> players;
    protected HashMap<Integer, Team> teams;

    protected boolean countDown;
    protected boolean start;
    protected boolean end;
    protected boolean forced;

    protected long startTime;
    protected long endTime;


    protected HashMap<String, Object> customs;


    public Game(int id, String name, World world, int nbTeams, int nbTeamsPlayers, int category) {
        new Game(id, name, world, nbTeams, nbTeamsPlayers, category, -1, -1);
    }
    public Game(int id, String name, World world, int nbTeams, int nbTeamsPlayers, int category, int subCategory) {
        new Game(id, name, world, nbTeams, nbTeamsPlayers, category, subCategory, -1);
    }
    public Game(int id, String name, World world, int nbTeams, int nbTeamsPlayers, int category, int subCategory, int subSubCategory) {
        this.id = id;
        this.name = name;

        this.world = world;

        this.nbTeams = nbTeams;
        this.nbTeamsPlayers = nbTeamsPlayers;

        this.category = category;
        this.subCategory = subCategory;
        this.subSubCategory = subSubCategory;

        this.players = new HashMap<>();
        this.teams = new HashMap<>();

        this.countDown = false;
        this.start = false;
        this.end = false;
        this.forced = false;

        this.startTime = 0;
        this.endTime = 0;

        this.customs = new HashMap<>();
    }



    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


    public World getWorld() {
        return world;
    }
    public void setWorld(World world) {
        this.world = world;
    }


    public int getNbTeams() {
        return nbTeams;
    }
    public void setNbTeams(int nbTeams) {
        this.nbTeams = nbTeams;
    }

    public int getNbTeamsPlayers() {
        return nbTeamsPlayers;
    }
    public void setNbTeamsPlayers(int nbTeamsPlayers) {
        this.nbTeamsPlayers = nbTeamsPlayers;
    }


    public int getCategory() {
        return category;
    }
    public void setCategory(int category) {
        this.category = category;
    }

    public int getSubCategory() {
        return subCategory;
    }
    public void setSubCategory(int subCategory) {
        this.subCategory = subCategory;
    }

    public int getSubSubCategory() {
        return subSubCategory;
    }
    public void setSubSubCategory(int subSubCategory) {
        this.subSubCategory = subSubCategory;
    }


    public HashMap<Player, GamesPlayer> getPlayers() {
        return players;
    }
    public void setPlayers(HashMap<Player, GamesPlayer> players) {
        this.players = players;
    }

    public HashMap<Integer, Team> getTeams() {
        return teams;
    }
    public void setTeams(HashMap<Integer, Team> teams) {
        this.teams = teams;
    }


    public boolean isCountDown() {
        return countDown;
    }
    public void setCountDown(boolean countDown) {
        this.countDown = countDown;
    }

    public boolean isStart() {
        return start;
    }
    public void setStart(boolean start) {
        this.start = start;
    }

    public boolean isEnd() {
        return end;
    }
    public void setEnd(boolean end) {
        this.end = end;
    }

    public boolean isForced() {
        return forced;
    }
    public void setForced(boolean forced) {
        this.forced = forced;
    }


    public long getStartTime() {
        return startTime;
    }
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }


    public HashMap<String, Object> getCustoms() {
        return customs;
    }
    public void setCustoms(HashMap<String, Object> customs) {
        this.customs = customs;
    }
}
