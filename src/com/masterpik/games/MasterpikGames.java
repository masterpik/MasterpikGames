package com.masterpik.games;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.masterpik.api.players.MasterpikPlayer;
import com.masterpik.api.players.Players;
import com.masterpik.games.chat.ChatManager;
import com.masterpik.games.games.GamesManager;
import com.masterpik.games.listeners.Listeners;
import com.masterpik.games.players.GamesPlayer;
import com.masterpik.games.players.PlayersManager;
import com.masterpik.games.scoreboard.ScoreboardManager;
import com.masterpik.games.settings.Settings;
import com.masterpik.games.teams.TeamsManager;
import com.masterpik.games.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class MasterpikGames extends JavaPlugin {

    private static MasterpikGames plugin;
    private static ProtocolManager protocolManager;


    @Override
    public void onLoad() {

    }

    @Override
    public void onEnable() {
        plugin = this;
        protocolManager = ProtocolLibrary.getProtocolManager();


        PlayersManager.init();
        GamesManager.init();
        TeamsManager.init();

        ScoreboardManager.init();

        ChatManager.init();

        Listeners.init();

        Utils.init();

        Settings.init();
    }

    @Override
    public void onDisable() {

    }

    public static ProtocolManager getProtocolManager() {
        return protocolManager;
    }

    public static MasterpikGames getPlugin() {
        return plugin;
    }


    public static GamesPlayer get(String player) {
        if (Bukkit.getPlayer(player) != null) {
            return get(Bukkit.getPlayer(player));
        } else {
            return null;
        }
    }
    public static GamesPlayer get(Player player) {
        if (!Players.players.containsKey(player.getUniqueId())) {
            Players.playerJoin(player.getUniqueId());
            PlayersManager.playerJoin(player.getUniqueId());
        }

        if (!PlayersManager.players.containsKey(player.getUniqueId())) {
            PlayersManager.playerJoin(player.getUniqueId());
        }

        return PlayersManager.players.get(player.getUniqueId());
    }
}
